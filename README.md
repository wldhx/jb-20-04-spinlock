# spinlock

A trivial spinlock built on `std::atomic`. Header-only.

Observe CI at `.gitlab-ci.yml` for build instructions.
