#include "libspinlock.hpp"
#include <cassert>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>

int a = 0;
Spinlock a_mutex;

void increment(int id) {
  for (int i = 0; i < 10; ++i) {
    a_mutex.lock();
    auto a_ = a;
    std::this_thread::sleep_for(std::chrono::nanoseconds(10));
    a = a_ + 1;
    std::cout << id << " => " << a << '\n';
    a_mutex.unlock();
  }
}

int main() {
  std::thread t1(increment, 1);
  std::thread t2(increment, 2);
  std::thread t3(increment, 3);
  std::thread t4(increment, 4);
  t1.join();
  t2.join();
  t3.join();
  t4.join();
  assert(a == 40);
}
