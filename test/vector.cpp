#include "libspinlock.hpp"
#include <cassert>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

std::vector<int> a{};
const std::vector<int> reference{0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                                 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
Spinlock a_mutex;

void append(int id) {
  a_mutex.lock();
  for (int i = 0; i < 10; ++i) {
    std::this_thread::sleep_for(std::chrono::nanoseconds(10));
    a.push_back(i);
  }
  a_mutex.unlock();
}

int main() {
  std::thread t1(append, 1);
  std::thread t2(append, 2);
  t1.join();
  t2.join();
  assert(a == reference);
}
