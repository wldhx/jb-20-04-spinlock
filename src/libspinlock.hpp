#pragma once

#include <atomic>

// Lockable
class Spinlock {
  std::atomic<bool> state;

public:
  Spinlock() noexcept = default;
  Spinlock(const Spinlock &) = delete;
  Spinlock &operator=(const Spinlock &) = delete;

  void lock() {
    while (state.exchange(true) == true) {
    }
  };
  bool try_lock() {
    if (state == true) {
      return false;
    }
    state = true;
    return true;
  }
  void unlock() { state = false; };
};
