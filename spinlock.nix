{ stdenv, cmake }:

stdenv.mkDerivation {
  name = "spinlock";
  src = ./src;

  nativeBuildInputs = [ cmake ];
}
